
# ttcal: Timetable Calendar

A TUI app for viewing ICalendar files and directories in a weekly grid format.

![A screenshot showing a calendar as columns of weeks, with events in colourful boxes spelling out TTCAL](screenshot.png)

## Compilation

ttcal is written is Rust and can be compiled by the standard Rust process.

    $ cargo build --release

The executable will be `target/release/ttcal`.

Or run directly

    $ cargo run <ics-file-or-directory.ics> …

## Usage

Run with

    $ ttcal <ics-file-or-directory.ics> …

Press `?` once running for a list of keybindings.

Each argument is treated as a single calendar (even if a directory). You can pass multiple calendar arguments.
