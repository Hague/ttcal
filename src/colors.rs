
use getset::Getters;
use ratatui::style::{Color, Style};
use termbg::Theme;

const LIGHT_GREEN : Color = Color::Rgb(169, 255, 160);
const LIGHT_BLUE : Color = Color::Rgb(176, 227, 255);
const LIGHT_YELLOW : Color = Color::Rgb(255, 255, 144);
const LIGHT_BROWN : Color = Color::Rgb(255, 208, 129);
const LIGHT_PURPLE : Color = Color::Rgb(255, 124, 255);
const LIGHT_RED : Color = Color::Rgb(255, 154, 154);
const LIGHT_GRAY : Color = Color::Rgb(235, 235, 235);

const DARK_GREEN : Color = Color::Rgb(0, 85, 0);
const DARK_BLUE : Color = Color::Rgb(40, 90, 130);
const DARK_YELLOW : Color = Color::Rgb(122, 130, 0);
const DARK_BROWN : Color = Color::Rgb(96, 64, 00);
const DARK_PURPLE : Color = Color::Rgb(134, 0, 139);
const DARK_RED : Color = Color::Rgb(126, 39, 47);
const DARK_GRAY : Color = Color::Rgb(33, 33, 33);
const MID_DARK_GRAY : Color = Color::Rgb(60, 60, 60);

#[derive(Getters)]
pub struct Colors {
    #[getset(get="pub")]
    calendar_styles : [[Style; 2]; 6],

    #[getset(get="pub")]
    cursor_style : Style,

    #[getset(get="pub")]
    now_style : Style,

    #[getset(get="pub")]
    not_match_style : Style,
}

impl Colors {
    pub fn new(theme : Theme) -> Self {
        match theme {
            Theme::Light=> Self {
                calendar_styles: [
                    // (active, inactive)
                    [
                        Style::default().bg(LIGHT_BLUE).fg(Color::Black),
                        Style::default().fg(LIGHT_BLUE),
                    ],
                    [
                        // Light Green
                        Style::default().bg(LIGHT_GREEN).fg(Color::Black),
                        Style::default().fg(LIGHT_GREEN),
                    ],
                    [
                        Style::default().bg(LIGHT_YELLOW).fg(Color::Black),
                        Style::default().fg(LIGHT_YELLOW),
                    ],
                    [
                        Style::default().bg(LIGHT_BROWN).fg(Color::Black),
                        Style::default().fg(LIGHT_BROWN),
                    ],
                    [
                        Style::default().bg(LIGHT_PURPLE).fg(Color::Black),
                        Style::default().fg(LIGHT_PURPLE),
                    ],
                    [
                        Style::default().bg(LIGHT_RED).fg(Color::Black),
                        Style::default().fg(LIGHT_RED),
                    ],
                ],
                cursor_style: Style::default()
                    .bg(Color::Gray)
                    .fg(Color::Black),
                now_style: Style::default().fg(Color::Red),
                not_match_style: Style::default()
                    .bg(LIGHT_GRAY)
                    .fg(Color::Black),
            },
            Theme::Dark => Self {
                calendar_styles: [
                    // (active, inactive)
                    [
                        Style::default().bg(DARK_BLUE).fg(Color::White),
                        Style::default().fg(DARK_BLUE),
                    ],
                    [
                        Style::default().bg(DARK_GREEN).fg(Color::White),
                        Style::default().fg(DARK_GREEN),
                    ],
                    [
                        Style::default().bg(DARK_YELLOW).fg(Color::White),
                        Style::default().fg(DARK_YELLOW),
                    ],
                    [
                        Style::default().bg(DARK_BROWN).fg(Color::White),
                        Style::default().fg(DARK_BROWN),
                    ],
                    [
                        Style::default().bg(DARK_PURPLE).fg(Color::White),
                        Style::default().fg(DARK_PURPLE),
                    ],
                    [
                        Style::default().bg(DARK_RED).fg(Color::White),
                        Style::default().fg(DARK_RED),
                    ],
                ],
                cursor_style: Style::default()
                    .bg(MID_DARK_GRAY)
                    .fg(Color::White),
                now_style: Style::default().fg(Color::Red),
                not_match_style: Style::default()
                    .bg(DARK_GRAY)
                    .fg(Color::White),
            },
        }
    }
}

