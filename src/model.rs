
use chrono::naive::NaiveDate;
use chrono::offset::Local;
use chrono::{DateTime, Datelike, Days, TimeDelta, TimeZone, Timelike, Weekday};
use getset::Getters;
use icalendar::Component;
use icalendar::Event;
use petgraph::algo::tarjan_scc;
use petgraph::graph::{Graph, NodeIndex};
use petgraph::Undirected;
use rrule::RRuleSet;
use std::cmp::{self, Ordering};
use std::io;
use std::ops::{Add, Sub};
use std::rc::Rc;

use crate::calendar::{
    dateperhapstime_to_local_datetime,
    get_recurring_event_start,
    Calendar,
    CalendarEvent,
    CalendarResources
};

pub const NUM_HOURS_PER_DAY : usize = 24;
/// Mon-Fri
const NUM_DAYS_WEEK: usize = 5;
pub const NUM_DAYS_FULL: usize = 7;

// 48 events would already fill the whole date, 100 would be overkill
const RECURRING_LIMIT : u16 = 100;
pub const MAX_ZOOM : usize = 4;

#[derive(Clone, PartialEq)]
pub enum Message {
    CloseCalendarList,
    CloseHelp,
    CloseViewEvent,
    ScrollWeekDown,
    ScrollWeekUp,
    CursorUp,
    CursorDown,
    CursorLeft,
    CursorRight,
    OpenCalendarList,
    OpenHelp,
    OpenViewEvent,
    ToggleInfoRow,
    CalendarListCursorUp,
    CalendarListCursorDown,
    ToggleCalendarActive,
    ToggleFullWeek,
    /// Start tracking, will be cancelled by week scrolls
    StartTrackingMode,
    StopTrackingMode,
    /// Update e.g. cursor pos due to passage of time
    Refresh,
    /// All events containing this string should be highlighted
    /// If the string is empty the search term will be cleared
    SetSearchTerm(String),
    ClearSearchTerm,
    NextZoom,
    Quit,
}

#[derive(Eq, Copy, Clone, Hash, PartialEq)]
pub enum DisplayMode {
    Timetable,
    Event,
    CalendarList,
    Help,
}

pub struct ModelResources {
    calendar_resources : CalendarResources,
}

#[derive(Default)]
pub struct CursorPosition {
    pub day_idx : usize,
    pub hour_row : usize,
    // if larger than num events, take as last event or entire cell if no events
    pub event_idx : usize,
}

#[derive(Getters)]
pub struct Model<'a> {
    pub calendar : Calendar<'a>,
    pub show_info_row : bool,

    #[getset(get="pub")]
    current_date : NaiveDate,

    #[getset(get="pub")]
    cursor_pos : CursorPosition,

    #[getset(get="pub")]
    display_mode : DisplayMode,

    #[getset(get="pub")]
    calendar_list_cursor_position : usize,

    #[getset(get="pub")]
    num_days_visible : usize,

    /// Whether view should track current time
    #[getset(get="pub")]
    is_tracking_mode : bool,

    /// Highlight all events that match this term
    #[getset(get="pub")]
    search_term : Option<String>,

    #[getset(get="pub")]
    zoom_level : usize,

    is_calendar_active : Vec<bool>,
    events_matrix_cache : Vec<Rc<Vec<Vec<Option<CalendarEvent<'a>>>>>>,
}

impl CursorPosition {
    pub fn new(day_idx: usize, hour_row: usize, event_idx: usize) -> Self {
        Self { day_idx, hour_row, event_idx }
    }

    /// Cursor should be considered at this position
    ///
    /// Needs num_events because if cursor past last event, consider last event
    /// selected.
    pub fn equals(
        &self,
        day_idx: usize,
        hour_row: usize,
        event_idx: usize,
    ) -> bool {
        self.day_idx == day_idx
            && self.hour_row == hour_row
            && self.event_idx == event_idx
    }
}

impl<'a> Model<'a> {
    pub fn default_resources() -> ModelResources {
        ModelResources {
            calendar_resources: Calendar::default_resources(),
        }
    }

    pub fn get_resources_from_files(
        filenames: &[String],
    ) -> io::Result<ModelResources> {
        let calendar_resources
            = Calendar::get_resources_from_files(filenames)?;
        Ok(ModelResources { calendar_resources })
    }

    pub fn new(resources: &'a ModelResources) -> Self {
        let mut res = Self {
            calendar: Calendar::new(&resources.calendar_resources),
            current_date: get_current_week_start(),
            cursor_pos: CursorPosition::default(),
            show_info_row: true,
            display_mode: DisplayMode::Timetable,
            calendar_list_cursor_position: 0,
            is_calendar_active: Vec::new(),
            events_matrix_cache: Vec::new(),
            num_days_visible: num_days_visible_for_current_day(),
            is_tracking_mode: true,
            search_term: None,
            zoom_level: 3,
        };
        res.cursor_pos = res.get_now_cursor_pos();
        res.create_events_matrix_cache();
        res
    }

    pub fn num_hour_rows(&self) -> usize {
        self.zoom_level * NUM_HOURS_PER_DAY
    }

    /// True if the calendar at the given index should be displayed
    pub fn is_calendar_active(&self, calendar_idx : usize) -> bool {
        let num_cals = self.calendar.get_calendars().len();
        calendar_idx < num_cals
            && (
                calendar_idx >= self.is_calendar_active.len()
                || self.is_calendar_active[calendar_idx]
            )
    }

    pub fn update(&mut self, msg: Message) -> Option<Message> {
        if msg == Message::StopTrackingMode {
            self.is_tracking_mode = false;
        } else if msg == Message::StartTrackingMode {
            self.is_tracking_mode = true;
        }

        if self.is_tracking_mode {
            self.set_current_date(get_current_week_start());
            self.num_days_visible = num_days_visible_for_current_day();
            self.cursor_pos = self.get_now_cursor_pos();
        }

        match msg {
            Message::ScrollWeekUp => {
                self.set_current_date(self.current_date.add(Days::new(7)));
                return Some(Message::StopTrackingMode);
            }
            Message::ScrollWeekDown => {
                self.set_current_date(self.current_date.sub(Days::new(7)));
                return Some(Message::StopTrackingMode);
            }
            Message::CursorUp => {
                if self.cursor_pos.hour_row > 0 {
                    self.cursor_pos.hour_row -= 1;
                    let last_event_idx
                        = self.get_last_events_idx_at_cursor_hour();
                    if self.cursor_pos.event_idx > last_event_idx {
                        self.cursor_pos.event_idx = last_event_idx;
                    }
                }
                return Some(Message::StopTrackingMode);
            }
            Message::CursorDown => {
                if self.cursor_pos.hour_row < self.num_hour_rows() - 1 {
                    self.cursor_pos.hour_row += 1;
                    let last_event_idx
                        = self.get_last_events_idx_at_cursor_hour();
                    if self.cursor_pos.event_idx > last_event_idx {
                        self.cursor_pos.event_idx = last_event_idx;
                    }
                }
                return Some(Message::StopTrackingMode);
            }
            Message::CursorLeft => {
                let last_event_idx = self.get_last_events_idx_at_cursor_hour();
                let real_event_idx = cmp::min(
                    self.cursor_pos.event_idx, last_event_idx
                );

                if real_event_idx > 0 {
                    self.cursor_pos.event_idx = real_event_idx - 1;
                } else if self.cursor_pos.day_idx > 0 {
                    self.cursor_pos.day_idx -= 1;
                    let last_event_idx
                        = self.get_last_events_idx_at_cursor_hour();
                    self.cursor_pos.event_idx = last_event_idx;
                }
                return Some(Message::StopTrackingMode);
            }
            Message::CursorRight => {
                let last_event_idx = self.get_last_events_idx_at_cursor_hour();
                let real_event_idx = cmp::min(
                    self.cursor_pos.event_idx, last_event_idx
                );

                if real_event_idx < last_event_idx {
                    self.cursor_pos.event_idx = real_event_idx + 1;
                } else if self.cursor_pos.day_idx < self.num_days_visible - 1 {
                    self.cursor_pos.day_idx += 1;
                    self.cursor_pos.event_idx = 0;
                }
                return Some(Message::StopTrackingMode);
            }
            Message::ToggleInfoRow => {
                self.show_info_row = !self.show_info_row;
                return None;
            }
            Message::OpenViewEvent => {
                if self.get_event_under_cursor().is_some() {
                    self.display_mode = DisplayMode::Event;
                }
                return None;
            }
            Message::CloseViewEvent => {
                self.display_mode = DisplayMode::Timetable;
                return None;
            }
            Message::OpenCalendarList => {
                self.display_mode = DisplayMode::CalendarList;
                return None;
            }
            Message::CloseCalendarList => {
                self.display_mode = DisplayMode::Timetable;
                return None;
            }
            Message::CalendarListCursorUp => {
                self.calendar_list_cursor_position
                    = self.calendar_list_cursor_position
                        .checked_sub(1)
                        .unwrap_or(0);
                return None;
            }
            Message::CalendarListCursorDown => {
                self.calendar_list_cursor_position
                    = self.calendar_list_cursor_position
                        .checked_add(1)
                        .unwrap_or(usize::MAX);
                let last_idx = self.calendar
                    .get_calendars()
                    .len()
                    .checked_sub(1)
                    .unwrap_or(0);
                self.calendar_list_cursor_position = cmp::min(
                    self.calendar_list_cursor_position,
                    last_idx
                );
                return None;
            }
            Message::ToggleCalendarActive => {
                let idx = self.calendar_list_cursor_position;
                if idx >= self.is_calendar_active.len() {
                    self.is_calendar_active.resize(idx + 1, true);
                }
                self.is_calendar_active[idx] = !self.is_calendar_active[idx];
                self.create_events_matrix_cache();
                return Some(Message::CalendarListCursorDown);
            }
            Message::ToggleFullWeek => {
                self.set_num_days_visible(
                    if self.num_days_visible == NUM_DAYS_WEEK {
                        NUM_DAYS_FULL
                    } else {
                        NUM_DAYS_WEEK
                    }
                );
                return None;
            }
            Message::OpenHelp => {
                self.display_mode = DisplayMode::Help;
                return None;
            }
            Message::CloseHelp => {
                self.display_mode = DisplayMode::Timetable;
                return None;
            }
            Message::SetSearchTerm(term) => {
                self.search_term
                    = if term.len() > 0 { Some(term) } else { None };
                return None;
            }
            Message::ClearSearchTerm => {
                self.search_term = None;
                return None;
            }
            Message::NextZoom => {
                let next_zoom = (self.zoom_level % MAX_ZOOM) + 1;
                self.set_zoom_level(next_zoom);
                return None;
            }
            _ => {
                // do nothing
                return None;
            }
        }
    }

    /// Get list of events for a given date
    ///
    /// 48 rows -- half hourly from 00:00 to 23:30. Organised into a layout
    /// of cells, so each event appears in the same cell position on each
    /// row it occupies, and each set of connected (by a spanning event) rows
    /// have the same len. May have None in some cells to help alignment with
    /// other rows.
    pub fn get_events_matrix(
        &self, date : &NaiveDate
    ) -> Rc<Vec<Vec<Option<CalendarEvent<'a>>>>> {
        let day_idx = date.signed_duration_since(self.current_date).num_days();
        if 0 <= day_idx && day_idx < (self.events_matrix_cache.len() as i64) {
            return Rc::clone(&self.events_matrix_cache[day_idx as usize]);
        }
        return self.make_events_matrix(date);
    }

    pub fn get_event_under_cursor(&self) -> Option<CalendarEvent> {
        let cursor = self.cursor_pos();
        let day = cursor.day_idx;
        let hour_row = cursor.hour_row;
        let event_idx = cursor.event_idx;

        let day_date = self.current_date().add(Days::new(day as u64));
        let matrix = self.get_events_matrix(&day_date);

        if hour_row < matrix.len() && matrix[hour_row].len() > 0 {
            let row = &matrix[hour_row];
            return row[cmp::min(event_idx, row.len() - 1)];
        } else {
            return None;
        }
    }

    /// Get the rows of the day the event occupies
    /// date is the date of the day (start/end of event might be different)
    pub fn get_event_indices(
        &self, date : &NaiveDate, event : &CalendarEvent
    ) -> (Option<usize>, Option<usize>) {
        let mut start = self.get_opt_local_datetime_index(
            date, &event.get_occurrence_start(), false
        );
        let mut end = self.get_opt_local_datetime_index(
            date, &event.get_occurrence_end(), true
        );
        if let Some(start_time) = event.dt_start() {
            start = Some(self.get_local_datetime_index(
                date,
                &start_time.with_timezone(&rrule::Tz::LOCAL),
                false,
            ));
        }
        if let Some(end_time) = event.dt_end() {
            end = Some(self.get_local_datetime_index(
                date,
                &end_time.with_timezone(&rrule::Tz::LOCAL),
                true,
            ));
        }
        (start, end)
    }

    /// Convenience for get_local_datetime_index with optional date
    pub fn get_opt_local_datetime_index<T : TimeZone>(
        &self,
        date : &NaiveDate,
        datetime : &Option<DateTime<T>>,
        round_up : bool
    ) -> Option<usize> {
        datetime.as_ref().map(|d|
            self.get_local_datetime_index(date, d, round_up)
        )
    }

    /// The row of the hours matrix for a given datetime
    ///
    /// Round up for end times, round down for start times, so complete
    /// time period is covered by indices
    ///
    /// date is the date to compute the indices for (which might be
    /// different to the start/end date of the event, but assumes event spans
    /// date)
    pub fn get_local_datetime_index<T : TimeZone>(
        &self, date : &NaiveDate, datetime : &DateTime<T>, round_up : bool
    ) -> usize {
        if *date != datetime.date_naive() {
            return if round_up { self.num_hour_rows() - 1 } else { 0 };
        } else {
            let time = datetime.time();
            let slot_mins = 60 / self.zoom_level;
            let round_adjust = (
                round_up && ((time.minute() as usize) % slot_mins) > 0
            ) as usize;
            return usize::try_from(
                self.zoom_level * (time.hour() as usize)
                    + ((time.minute() as usize) / slot_mins)
            ).unwrap() + round_adjust;
        }
    }

    pub fn get_now_cursor_pos(&self) -> CursorPosition {
        let now = &Local::now();
        return CursorPosition::new(
            now.weekday().num_days_from_monday() as usize,
            self.get_local_datetime_index(&now.date_naive(), now, false),
            0
        )
    }

    /// Set number of rows per hour
    pub fn set_zoom_level(&mut self, new_zoom : usize) {
        if 0 < new_zoom && new_zoom <= MAX_ZOOM {
            if !self.is_tracking_mode {
                self.cursor_pos.hour_row = translate_hour_row(
                    self.cursor_pos.hour_row,
                    self.zoom_level,
                    new_zoom,
                );
            }
            self.zoom_level = new_zoom;
            // adjust for slight offsets in now time
            if self.is_tracking_mode {
                self.cursor_pos = self.get_now_cursor_pos();
            }
            self.create_events_matrix_cache();
        }
    }

    /// The index of the last event at day/hour_row of cursor
    ///
    /// 0 might mean no events or 1 element
    fn get_last_events_idx_at_cursor_hour(&self) -> usize {
        let day_date = self.current_date().add(
            Days::new(self.cursor_pos.day_idx as u64)
        );
        let matrix = self.get_events_matrix(&day_date);
        matrix[self.cursor_pos.hour_row].len().checked_sub(1).unwrap_or(0)
    }

    fn set_current_date(&mut self, current_date : NaiveDate) {
        if self.current_date != current_date {
            self.current_date = current_date;
            self.create_events_matrix_cache();
        }
    }

    fn create_events_matrix_cache(&mut self) {
        self.events_matrix_cache.clear();
        for day_idx in 0..NUM_DAYS_FULL {
            let date = self.current_date.add(Days::new(day_idx as u64));
            self.events_matrix_cache.push(self.make_events_matrix(&date));
        }
    }

    fn make_events_matrix(
        &self, date : &NaiveDate
    ) -> Rc<Vec<Vec<Option<CalendarEvent<'a>>>>> {
        let day_events = self.get_sorted_events(date);

        let mut matrix : Vec<Vec<Option<CalendarEvent<'a>>>>
            = (0..self.num_hour_rows()).map(|_| Vec::new()).collect();

        // All "interconnected" rows (sharing events) should have same
        // the same width to ensure alignment. Make a graph where rows
        // are nodes and edges mean a shared event. SCCs should have
        // same width. Build graph while lining up events below, then
        // equalise lengths in second pass.
        let mut row_graph = Graph::new_undirected();
        let row_nodes : Vec<_> = matrix.iter()
            .enumerate()
            .map(|(idx, _)| row_graph.add_node(idx))
            .collect();

        // relies on adding events in sorted order
        for event in day_events {
            self.add_event_to_matrix_and_graph(
                date,
                &event,
                &row_nodes,
                &mut matrix,
                &mut row_graph,
            );
        }

        self.pad_matrix_from_graph(&row_graph, &mut matrix);

        return Rc::new(matrix);
    }

    fn get_sorted_events(&self, date : &NaiveDate) -> Vec<CalendarEvent<'a>> {
        let mut day_events : Vec<CalendarEvent> = Vec::new();

        self.add_single_events_to_vector(date, &mut day_events);
        self.add_recurring_events_to_vector(date, &mut day_events);

        day_events.sort_by(|e1, e2| {
            let e1_start = &e1.get_occurrence_start();
            let e2_start = &e2.get_occurrence_start();
            match e1_start.cmp(&e2_start) {
                Ordering::Equal => {
                    match e1.calendar_index().cmp(&e2.calendar_index()) {
                        Ordering::Equal => {
                            e1.event().get_summary().cmp(
                                &e2.event().get_summary()
                            )
                        }
                        order => order
                    }
                }
                order => order
            }
        });

        //let iyidx = day_events.iter().position(
        //    |e| e.event().get_summary() == Some("IY3501/LEC/01/01")
        //).unwrap_or(0);

        //let csidx = day_events.iter().position(
        //    |e| e.event().get_summary() == Some("ALL CS1890/PRAC/01/02\\, CS2890/PRAC/01/02")
        //).unwrap_or(0);

        //if iyidx > 0 && csidx > 0 {
        //    let e1_start = dateperhapstime_to_local_datetime(
        //        &day_events[csidx].event().get_start()
        //    );
        //    let e2_start = dateperhapstime_to_local_datetime(
        //        &day_events[iyidx].event().get_start()
        //    );
        //    println!("Huu {:?} {:?} {:?}", e1_start.cmp(&e2_start), e1_start, e2_start);
        //}

        return day_events;
    }

    fn add_recurring_events_to_vector(
        &self,
        date : &NaiveDate,
        vector : &mut Vec<CalendarEvent<'a>>,
    ) {
        let local_start = date
            .and_hms_opt(0, 0, 0)
            .unwrap()
            .and_local_timezone(rrule::Tz::Local(Local))
            .unwrap();
        let local_end = local_start.add(Days::new(1));

        for r_event in self.calendar.recurring_events() {
            let c_event = r_event.calendar_event();
            if !self.is_calendar_active(c_event.calendar_index()) {
                continue;
            }

            let event = r_event.calendar_event().event();
            let duration = get_duration(event);
            // Use start in proper timezone, then convert to local in matrix
            // Theory: recurrence should only adjust for daylight saving if
            // it is in a timezone where it applies.
            let dt_start = get_recurring_event_start(event);
            if let Some(dt_start) = dt_start {
                let ruleset = RRuleSet::new(dt_start)
                    .rrule(r_event.rrule().clone())
                    .after(local_start)
                    .before(local_end)
                    .limit();
                for instance_start in ruleset.all(RECURRING_LIMIT).dates {
                    let instance_start
                        = instance_start.with_timezone(&rrule::Tz::LOCAL);
                    let instance_end = instance_start.add(duration);
                    let full_event = CalendarEvent::with_times(
                        c_event, Some(instance_start), Some(instance_end)
                    );
                    vector.push(full_event);
                }
            }
        }
    }

    fn add_single_events_to_vector(
        &self,
        date : &NaiveDate,
        vector : &mut Vec<CalendarEvent<'a>>,
    ) {
        if let Some(events) = self.calendar.get_single_events(date) {
            for event in events {
                if self.is_calendar_active(event.calendar_index()) {
                    vector.push(*event);
                }
            }
        }
    }

    fn pad_matrix_from_graph(
        &self,
        row_graph : &Graph<usize, (), Undirected>,
        matrix : &mut Vec<Vec<Option<CalendarEvent<'a>>>>,
    ) {
        for scc in tarjan_scc(row_graph) {
            let rows : Vec<_> = scc.iter()
                .map(|n| row_graph.node_weight(*n).unwrap())
                .collect();
            let width = rows.iter()
                .map(|r| matrix[**r].len())
                .max()
                .unwrap_or(0);
            for row in rows {
                matrix[*row].resize_with(width, || None);
            }
        }
    }

    /// Add a single event to the data structures
    /// date is the day the matrix is for, which might not fully match
    /// start/end date of event
    fn add_event_to_matrix_and_graph(
        &self,
        date : &NaiveDate,
        event : &CalendarEvent<'a>,
        row_nodes : &Vec<NodeIndex>,
        matrix : &mut Vec<Vec<Option<CalendarEvent<'a>>>>,
        row_graph : &mut Graph<usize, (), Undirected>,
    ) {
        let (start, end) = self.get_event_indices(date, event);
        let start = start.unwrap_or(0);
        let mat_idx = matrix[start].iter()
            .position(|e| !e.is_some())
            .unwrap_or(matrix[start].len());

        let mut end = end.unwrap_or(self.num_hour_rows() - 1);
        if start == end {
            end += 1;
        }
        for i in cmp::max(0, start)..end {
            let row = &mut matrix[i as usize];
            if mat_idx >= row.len() {
                row.resize_with(mat_idx + 1, || None);
            }
            matrix[i as usize][mat_idx] = Some(*event);
            row_graph.update_edge(row_nodes[start], row_nodes[i], ());
        }
    }

    fn set_num_days_visible(&mut self, num_days_visible : usize) {
        self.num_days_visible = num_days_visible;
        self.create_events_matrix_cache();
    }
}

/// When zoom changes, update an hour row to the new equivalent
pub fn translate_hour_row(
    hour_row : usize, old_zoom : usize, new_zoom : usize
) -> usize {
    if new_zoom > old_zoom {
        let num_extra_per_hour = new_zoom - old_zoom;
        let old_hour = hour_row / old_zoom;
        return hour_row + old_hour * num_extra_per_hour;
    } else {
        let num_less_per_hour = old_zoom - new_zoom;
        let old_hour = hour_row / old_zoom;
        return hour_row - old_hour * num_less_per_hour;
    }
}

/// Get start date of current week
fn get_current_week_start() -> NaiveDate {
    let today = Local::now().date_naive();
    let days_off = today.weekday().num_days_from_monday();
    today.sub(Days::new(days_off.into()))
}

fn get_duration(event : &Event) -> TimeDelta {
    let start = dateperhapstime_to_local_datetime(&event.get_start());
    if let Some(start) = start {
        let end = dateperhapstime_to_local_datetime(&event.get_end());
        if let Some(end) = end {
            return end - start;
        } else {
            return TimeDelta::new(0, 0).unwrap();
        }
    } else {
        return TimeDelta::new(0, 0).unwrap();
    }
}

/// Number of days visible needed to view current day
fn num_days_visible_for_current_day() -> usize {
    let is_weekend = Local::now().weekday().num_days_from_monday()
        > Weekday::Fri.num_days_from_monday();
    if is_weekend {
        NUM_DAYS_FULL
    } else {
        NUM_DAYS_WEEK
    }
}

