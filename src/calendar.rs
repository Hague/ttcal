
use chrono::naive::NaiveDate;
use chrono::{Datelike, DateTime, Days, Local, TimeZone};
use getset::{CopyGetters, Getters};
use glob::MatchOptions;
use glob::glob_with;
use icalendar::Calendar as ICalendar;
use icalendar::CalendarComponent;
use icalendar::CalendarDateTime::Floating;
use icalendar::CalendarDateTime::Utc;
use icalendar::CalendarDateTime::WithTimezone;
use icalendar::CalendarDateTime;
use icalendar::Component;
use icalendar::DatePerhapsTime::Date;
use icalendar::DatePerhapsTime::DateTime as IDateTime;
use icalendar::DatePerhapsTime;
use icalendar::Event;
use rrule::{RRule, Unvalidated, Validated};
use std::collections::HashMap;
use std::ops::{Add, Sub};
use std::path::Path;
use std::str::FromStr;
use std::{fs, io};

const RRULE_PROPERTY : &str = "RRULE";

pub struct ICalendarFile {
    pub ical : ICalendar,
    pub filename : String,
}

pub struct CalendarResources {
    pub icals : Vec<ICalendarFile>,
}

/// Holds a reference to an event and index of calendar it belongs to
#[derive(Clone, Copy, CopyGetters, Getters)]
pub struct CalendarEvent<'a> {
    #[getset(get_copy="pub")]
    calendar_index : usize,

    #[getset(get="pub")]
    event : &'a Event,

    /// If not None, override date start (e.g. recurring event)
    #[getset(get="pub")]
    dt_start : Option<DateTime<rrule::Tz>>,

    /// If not None, override date end (e.g. recurring event)
    #[getset(get="pub")]
    dt_end : Option<DateTime<rrule::Tz>>,
}

/// As CalendarEvent, but with parsed rrule
#[derive(Getters)]
pub struct RecurringCalendarEvent<'a> {
    #[getset(get="pub")]
    calendar_event : CalendarEvent<'a>,

    #[getset(get="pub")]
    rrule : RRule<Validated>,
}

#[derive(Getters)]
pub struct Calendar<'a> {
    resources : &'a CalendarResources,
    single_events : HashMap<NaiveDate, Vec<CalendarEvent<'a>>>,

    #[getset(get="pub")]
    recurring_events : Vec<RecurringCalendarEvent<'a>>,
}

impl<'a> CalendarEvent<'a> {
    pub fn new(
        calendar_index : usize,
        event : &'a Event,
        dt_start : Option<DateTime<rrule::Tz>>,
        dt_end : Option<DateTime<rrule::Tz>>,
    ) -> Self {
        Self { calendar_index, event, dt_start, dt_end }
    }

    /// Create a copy of event with a new dt_start and dt_end
    pub fn with_times(
        event : &CalendarEvent<'a>,
        dt_start : Option<DateTime<rrule::Tz>>,
        dt_end : Option<DateTime<rrule::Tz>>,
    ) -> Self {
        Self {
            calendar_index: event.calendar_index(),
            event: event.event,
            dt_start,
            dt_end,
        }
    }

    /// Get the start of this event in local timezone
    ///
    /// Applies dt_start override if appropriate
    pub fn get_occurrence_start(&self) -> Option<DateTime<Local>> {
        if self.dt_start.is_some() {
            self.dt_start.map(|d| d.with_timezone(&Local))
        } else {
            dateperhapstime_to_local_datetime(&self.event.get_start())
        }
    }

    /// Get the end of this event in local timezone
    ///
    /// Applies dt_end override if appropriate
    pub fn get_occurrence_end(&self) -> Option<DateTime<Local>> {
        if self.dt_end.is_some() {
            self.dt_end.map(|d| d.with_timezone(&Local))
        } else {
            dateperhapstime_to_local_datetime(&self.event.get_end())
        }
    }
}

impl<'a> Calendar<'a> {
    pub fn default_resources() -> CalendarResources {
        CalendarResources {
            icals: Vec::new(),
        }
    }

    pub fn get_resources_from_files(
        filenames : &[String],
    ) -> io::Result<CalendarResources> {
        let icals = filenames.iter().map(|filename| {
            let read = if Path::new(filename).is_file() {
                load_calendar_from_file(filename)
            } else {
                load_calendar_from_directory(filename)
            }?;
            Ok(ICalendarFile {
                ical: read,
                filename: filename.into(),
            })
        }).collect::<io::Result<Vec<ICalendarFile>>>()?;

        Ok(CalendarResources { icals })
    }

    pub fn new(resources : &'a CalendarResources) -> Self {
        let single_events = get_single_events(resources);
        let recurring_events = get_recurring_events(resources);
        Self { resources, single_events, recurring_events }
    }

    pub fn get_calendars(&self) -> &Vec<ICalendarFile> {
        &self.resources.icals
    }

    pub fn get_single_events(
        &self, date : &NaiveDate
    ) -> Option<&Vec<CalendarEvent<'a>>> {
        self.single_events.get(&date)
    }
}

fn datetime_to_naive_date(datetime : &CalendarDateTime) -> NaiveDate {
    let local_datetime = datetime_to_local_datetime(datetime);
    local_datetime.date_naive()
}

pub fn datetime_to_local_datetime(
    datetime : &CalendarDateTime
) -> DateTime<Local> {
    match datetime {
        Floating(naive_datetime) => {
            naive_datetime.and_local_timezone(Local).unwrap()
        }
        Utc(utc_datetime) => {
            DateTime::from(*utc_datetime)
        }
        WithTimezone{ date_time, tzid } => {
            if let Some(tz) = tzid.parse::<chrono_tz::Tz>().ok() {
                tz.from_local_datetime(date_time)
                    .unwrap()
                    .with_timezone(&Local)
            } else {
                date_time.and_local_timezone(Local).unwrap()
            }
        }
    }
}


pub fn dateperhapstime_to_local_datetime(
    datetime : &Option<DatePerhapsTime>
) -> Option<DateTime<Local>> {
    if let Some(datetime) = datetime {
        match datetime {
            Date(date) => Some(
                Local.with_ymd_and_hms(
                    date.year(), date.month(), date.day(), 0, 0, 0
                ).unwrap()
            ),
            IDateTime(datetime) => Some(datetime_to_local_datetime(&datetime)),
        }
    } else {
        return None;
    }
}

pub fn unescape_cal_string(s : &str) -> String {
    // unescape \\ \; \, \n \N -- permitted by RFC 5545
    let mut new = String::with_capacity(s.len());
    let mut cs = s.chars();
    while let Some(c) = cs.next() {
        if c == '\\' {
            if let Some(c) = cs.next() {
                if c == 'n' || c == 'N' {
                    new.push('\n');
                } else if c == ';' {
                    new.push(';');
                } else if c == ',' {
                    new.push(',');
                } else if c == '\\' {
                    new.push('\\');
                } else {
                    new.push('\\');
                    new.push(c);
                }
            } else {
                new.push('\\');
                new.push(c);
            }
        } else {
            new.push(c);
        }
    }
    return new;
}

fn load_calendar_from_file(filename : &String) -> io::Result<ICalendar> {
    let contents = fs::read_to_string(filename)?;
    match contents.parse() {
        Ok(read) => Ok(read),
        Err(error) => Err(io::Error::new(io::ErrorKind::Other, error)),
    }
}

fn load_calendar_from_directory(dirname : &String) -> io::Result<ICalendar> {
    let mut cal = ICalendar::new();
    let options = MatchOptions {
        case_sensitive: false,
        require_literal_separator: false,
        require_literal_leading_dot: false,
    };
    match glob_with(&format!("{0}/**/*.ics", dirname), options) {
        Ok(entries) => {
            for entry in entries {
                if let Ok(path) = entry {
                    let mut sub_cal = load_calendar_from_file(
                        &String::from(path.to_string_lossy())
                    )?;
                    cal.append(&mut sub_cal);
                }
            }
            Ok(cal)
        }
        Err(error) => Err(io::Error::new(io::ErrorKind::Other, error))
    }
}

fn get_single_events<'a>(
    resources : &'a CalendarResources
) -> HashMap<NaiveDate, Vec<CalendarEvent<'a>>> {
    let mut single_events = HashMap::new();
    for (idx, ical) in resources.icals.iter().enumerate() {
        for component in &ical.ical.components {
            if let CalendarComponent::Event(event) = component {
                if has_rrule(event) {
                    continue;
                }

                let start = event.get_start();
                let end = event.get_end();
                if !start.is_some() || !end.is_some() {
                    continue;
                }
                let start = start.unwrap();
                let end = end.unwrap();
                let start_date = match start {
                    Date(date) => date,
                    IDateTime(ref datetime) => datetime_to_naive_date(datetime),
                };
                let end_date = match end {
                    // end date is non-inclusive
                    Date(date) => date.sub(Days::new(1)),
                    IDateTime(ref datetime) => datetime_to_naive_date(datetime),
                };

                let mut ins_date = start_date;
                while ins_date <= end_date {
                    if !single_events.contains_key(&ins_date) {
                        single_events.insert(ins_date, Vec::new());
                    }
                    single_events.get_mut(&ins_date).unwrap().push(
                        CalendarEvent::new(idx, event, None, None),
                    );
                    ins_date = ins_date.add(Days::new(1));
                }
            }
        }
    }

    return single_events;
}

fn get_recurring_events<'a>(
    resources : &'a CalendarResources
) -> Vec<RecurringCalendarEvent<'a>> {
    let mut recurring_events = Vec::new();

    for (idx, ical) in resources.icals.iter().enumerate() {
        for component in &ical.ical.components {
            if let CalendarComponent::Event(event) = component {
                if let Some(rrule) = get_rrule(event) {
                    let dt_start = get_recurring_event_start(&event);
                    if let Some(dt_start) = dt_start {
                        if let Some(rrule) = rrule.validate(dt_start).ok() {
                            let calendar_event
                                = CalendarEvent::new(idx, &event, None, None);
                            recurring_events.push(
                                RecurringCalendarEvent {
                                    calendar_event,
                                    rrule,
                                }
                            );
                        }
                    }
                }
            }
        }
    }

    return recurring_events;
}

/// Return event start date in timezone event
///
/// Defaults to Local if problems parsing
pub fn get_recurring_event_start(
    event : &Event
) -> Option<DateTime<rrule::Tz>> {
    dateperhapstime_to_local_datetime(&event.get_start())
        .map(|dts| {
            if let Some(start) = event.get_start() {
                let tz = match start {
                    IDateTime(datetime) => {
                        match datetime {
                            Floating(_) => rrule::Tz::LOCAL,
                            Utc(_) => rrule::Tz::UTC,
                            WithTimezone{date_time: _, tzid} => {
                                let tz = chrono_tz::Tz::from_str(&tzid).ok();
                                if let Some(tz) = tz {
                                    rrule::Tz::Tz(tz)
                                } else {
                                    rrule::Tz::LOCAL
                                }
                            }
                        }
                    }
                    Date(_) => rrule::Tz::LOCAL
                };
                dts.with_timezone(&tz)
            } else {
                dts.with_timezone(&rrule::Tz::LOCAL)
            }
        })
}

fn has_rrule(event : &Event) -> bool {
    return event.properties().contains_key(RRULE_PROPERTY);
}

fn get_rrule(event : &Event) -> Option<RRule<Unvalidated>> {
    if let Some(rrule) = event.properties().get("RRULE") {
        return rrule.value().parse().ok();
    } else {
        return None;
    }
}
