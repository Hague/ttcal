
use crossterm::event::KeyEvent;
use crossterm::event::{
    KeyCode::{Char, Down, Enter, Esc, Home, Left, PageDown, PageUp, Right, Up},
    KeyEventKind::Press,
    KeyModifiers,
};
use once_cell::sync::Lazy;
use std::collections::HashMap;

use crate::model::{DisplayMode, Message as MMessage};
use crate::view::Message as VMessage;

#[derive(Clone)]
pub enum UpdateMsg {
    ModelMsg(MMessage),
    ViewMsg(VMessage),
    Reload,
}

/// Whether to enter the current input, or abort/quit input
#[derive(Clone)]
pub enum InputMsg {
    Enter,
    Quit
}

use UpdateMsg::{ModelMsg, ViewMsg, Reload};

macro_rules! key {
    ( $code:expr ) => {
        KeyEvent::new_with_kind($code, KeyModifiers::NONE, Press)
    };
    ( $mod:expr, $code:expr ) => {
        KeyEvent::new_with_kind($code, $mod, Press)
    };
}

pub static TEXT_INPUT_KEYMAP : Lazy<HashMap<KeyEvent, InputMsg>>
    = Lazy::new(|| {
        HashMap::from([
            (key!(Enter), InputMsg::Enter),
            (key!(KeyModifiers::CONTROL, Char('j')), InputMsg::Enter),
            (key!(Esc), InputMsg::Quit),
        ])
    });

pub static KEYMAP : Lazy<HashMap<DisplayMode, HashMap<KeyEvent, UpdateMsg>>>
    = Lazy::new(|| {
        HashMap::from([
            (
                DisplayMode::Timetable,
                HashMap::from([
                    (key!(Char('q')), ModelMsg(MMessage::Quit)),
                    (key!(Esc), ModelMsg(MMessage::Quit)),
                    (key!(Char(',')), ModelMsg(MMessage::Quit)),
                    (
                        key!(KeyModifiers::CONTROL, Char('h')),
                        ModelMsg(MMessage::Quit)
                    ),
                    (key!(Char('H')), ModelMsg(MMessage::ScrollWeekDown)),
                    (key!(PageDown), ModelMsg(MMessage::ScrollWeekDown)),
                    (key!(Char('L')), ModelMsg(MMessage::ScrollWeekUp)),
                    (key!(PageUp), ModelMsg(MMessage::ScrollWeekUp)),
                    (key!(Char('j')), ModelMsg(MMessage::CursorDown)),
                    (key!(Down), ModelMsg(MMessage::CursorDown)),
                    (key!(Char('k')), ModelMsg(MMessage::CursorUp)),
                    (key!(Up), ModelMsg(MMessage::CursorUp)),
                    (key!(Char('h')), ModelMsg(MMessage::CursorLeft)),
                    (key!(Left), ModelMsg(MMessage::CursorLeft)),
                    (key!(Char('l')), ModelMsg(MMessage::CursorRight)),
                    (key!(Right), ModelMsg(MMessage::CursorRight)),
                    (key!(Char('d')), ModelMsg(MMessage::ToggleInfoRow)),
                    (key!(Enter), ModelMsg(MMessage::OpenViewEvent)),
                    (
                        key!(KeyModifiers::CONTROL, Char('j')),
                        ModelMsg(MMessage::OpenViewEvent)
                    ),
                    (key!(Char('c')), ModelMsg(MMessage::OpenCalendarList)),
                    (key!(Char('v')), ModelMsg(MMessage::OpenCalendarList)),
                    (key!(Char('w')), ModelMsg(MMessage::ToggleFullWeek)),
                    (key!(Char('t')), ModelMsg(MMessage::StartTrackingMode)),
                    (key!(Home), ModelMsg(MMessage::StartTrackingMode)),
                    (key!(Char('?')), ModelMsg(MMessage::OpenHelp)),
                    (
                        key!(KeyModifiers::CONTROL, Char('c')),
                        ModelMsg(MMessage::Quit)
                    ),
                    (key!(Char('r')), Reload),
                    (key!(Char('/')), ViewMsg(VMessage::EnterSearchTerm)),
                    (key!(Char('z')), ModelMsg(MMessage::NextZoom)),
                ]),
            ),
            (
                DisplayMode::Event,
                HashMap::from([
                    (key!(Esc), ModelMsg(MMessage::CloseViewEvent)),
                    (key!(Char('q')), ModelMsg(MMessage::CloseViewEvent)),
                    (key!(Char(',')), ModelMsg(MMessage::CloseViewEvent)),
                    (
                        key!(KeyModifiers::CONTROL, Char('h')),
                        ModelMsg(MMessage::CloseViewEvent)
                    ),
                    (key!(Enter), ModelMsg(MMessage::CloseViewEvent)),
                    (
                        key!(KeyModifiers::CONTROL, Char('j')),
                        ModelMsg(MMessage::CloseViewEvent)
                    ),
                    (key!(Char('j')), ViewMsg(VMessage::ScrollDown)),
                    (key!(Down), ViewMsg(VMessage::ScrollDown)),
                    (key!(Char('k')), ViewMsg(VMessage::ScrollUp)),
                    (key!(Up), ViewMsg(VMessage::ScrollUp)),
                    (
                        key!(KeyModifiers::CONTROL, Char('c')),
                        ModelMsg(MMessage::Quit)
                    ),
                    (key!(Char('r')), Reload),
                ]),
            ),
            (
                DisplayMode::CalendarList,
                HashMap::from([
                    (key!(Esc), ModelMsg(MMessage::CloseCalendarList)),
                    (key!(Char('q')), ModelMsg(MMessage::CloseCalendarList)),
                    (key!(Char(',')), ModelMsg(MMessage::CloseCalendarList)),
                    (
                        key!(KeyModifiers::CONTROL, Char('h')),
                        ModelMsg(MMessage::CloseCalendarList)
                    ),
                    (key!(Char('c')), ModelMsg(MMessage::CloseCalendarList)),
                    (key!(Enter), ModelMsg(MMessage::ToggleCalendarActive)),
                    (
                        key!(KeyModifiers::CONTROL, Char('j')),
                        ModelMsg(MMessage::ToggleCalendarActive)
                    ),
                    (key!(Char(' ')), ModelMsg(MMessage::ToggleCalendarActive)),
                    (
                        key!(Char('j')),
                        ModelMsg(MMessage::CalendarListCursorDown)
                    ),
                    (key!(Char('k')), ModelMsg(MMessage::CalendarListCursorUp)),
                    (
                        key!(KeyModifiers::CONTROL, Char('c')),
                        ModelMsg(MMessage::Quit)
                    ),
                    (key!(Char('r')), Reload),
                ]),
            ),
            (
                DisplayMode::Help,
                HashMap::from([
                    (key!(Esc), ModelMsg(MMessage::CloseHelp)),
                    (key!(Char('q')), ModelMsg(MMessage::CloseHelp)),
                    (key!(Enter), ModelMsg(MMessage::CloseHelp)),
                    (
                        key!(KeyModifiers::CONTROL, Char('h')),
                        ModelMsg(MMessage::CloseHelp)
                    ),
                    (
                        key!(KeyModifiers::CONTROL, Char('j')),
                        ModelMsg(MMessage::CloseHelp)
                    ),
                    (
                        key!(KeyModifiers::CONTROL, Char('c')),
                        ModelMsg(MMessage::Quit)
                    ),
                ]),
            ),
        ])
    });
