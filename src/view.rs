
use chrono::offset::Local;
use chrono::{DateTime, Days, TimeZone};
use crossterm::event::{Event, KeyEvent};
use icalendar::DatePerhapsTime::{Date as IDate, DateTime as IDateTime};
use icalendar::{Component, DatePerhapsTime, EventLike};
use ratatui::{prelude::*, widgets::*};
use regex::Regex;
use std::cmp;
use std::fmt::Display;
use std::ops::Add;
use std::path::Path;
use std::rc::Rc;
use tui_input::Input;
use tui_input::backend::crossterm::EventHandler;

use crate::calendar::{
    dateperhapstime_to_local_datetime,
    unescape_cal_string,
    CalendarEvent,
    ICalendarFile
};
use crate::colors::Colors;
use crate::keymap::{InputMsg, TEXT_INPUT_KEYMAP};
use crate::model::{
    self,
    DisplayMode,
    MAX_ZOOM,
    Model,
    NUM_HOURS_PER_DAY
};

const MAX_CALENDAR_NAME_WIDTH_CHARS: usize = 15;
const TIME_WIDTH_CHARS: u16 = 5;
const DAYS: [&str; 7] = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
const INFO_ROW_SIZE: u16 = 6;
// how many hours to show at zoom level suggested
const SUGGEST_ZOOM_NUM_HOURS: u16 = 10;

#[derive(Clone, Copy, PartialEq)]
pub enum Message {
    ScrollUp,
    ScrollDown,
    EnterSearchTerm,
}

pub struct ViewState {
    last_display_mode : Option<DisplayMode>,
    event_description_scroll : u16,
    hour_row_offset : usize,
    is_entering_search_term : bool,
    search_term_input : Input,
    colors : Colors,
    last_zoom_level : usize,
    last_cursor_hour_row: usize,
}

impl ViewState {
    pub fn new(colors : Colors) -> Self {
        Self {
            last_display_mode: None,
            event_description_scroll: 0,
            hour_row_offset: 0,
            is_entering_search_term: false,
            search_term_input: Input::default(),
            colors,
            last_zoom_level: 0,
            last_cursor_hour_row: 0,
        }
    }

    pub fn update(
        &mut self, model : &Model, msg : Message
    ) -> Option<model::Message> {
        match model.display_mode() {
            DisplayMode::Event => self.update_event_mode(msg),
            DisplayMode::Timetable => self.update_timetable_mode(msg),
            _ => None,
        }
    }

    pub fn get_current_time_index(&self, model : &Model) -> Option<usize> {
        let now = &Local::now();
        model.get_local_datetime_index(&now.date_naive(), now, false)
            .checked_sub(self.hour_row_offset)
    }

    /// Set timetable scroll to match current time
    pub fn scroll_to_current_time(
        &mut self, model : &Model, num_hour_rows_visible : usize
    ) {
        let now = &Local::now();
        let cur_idx = model.get_local_datetime_index(
            &now.date_naive(), now, false
        );
        self.hour_row_offset = cmp::min(
            model.num_hour_rows()
                .checked_sub(num_hour_rows_visible)
                .unwrap_or(0),
            cur_idx.checked_sub(num_hour_rows_visible / 4).unwrap_or(0),
        );
    }

    /// Make sure the cursor is visible on screen
    pub fn scroll_so_cursor_visible(
        &mut self,
        num_hour_rows_visible : usize,
        cursor_row : usize,
    ) {
        self.hour_row_offset = cmp::min(self.hour_row_offset, cursor_row);

        let last_idx = self.hour_row_offset + num_hour_rows_visible;
        if last_idx <= cursor_row {
            self.hour_row_offset = cursor_row - num_hour_rows_visible + 1;
        }
    }

    /// If all input events should go directly to the view
    ///
    /// Rather than through this usual keybindings
    pub fn is_consuming_input(&self) -> bool {
        self.is_entering_search_term
    }

    /// If is_consuming_input, pass input events here
    ///
    /// May return a model message if update requires
    pub fn consume_input(
        &mut self, key : KeyEvent
    ) -> Option<model::Message> {
        if let Some(msg) = TEXT_INPUT_KEYMAP.get(&key) {
            match msg {
                InputMsg::Enter => {
                    self.is_entering_search_term = false;
                    let msg = model::Message::SetSearchTerm(
                        self.search_term_input.value().to_string()
                    );
                    return Some(msg);
                }
                InputMsg::Quit => {
                    self.is_entering_search_term = false;
                    return Some(model::Message::ClearSearchTerm);
                }
            }
        } else {
            self.search_term_input.handle_event(&Event::Key(key));
            return None;
        }
    }

    fn update_timetable_mode(
        &mut self, msg : Message
    ) -> Option<model::Message> {
        match msg {
            Message::EnterSearchTerm => {
                self.is_entering_search_term = true;
                return None;
            }
            _ => {
                /* pass */
                return None;
            }
        };
    }

    fn update_event_mode(
        &mut self, msg : Message
    ) -> Option<model::Message> {
        match msg {
            Message::ScrollUp => {
                self.event_description_scroll
                    = self.event_description_scroll.checked_sub(1).unwrap_or(0);
            }
            Message::ScrollDown => {
                self.event_description_scroll
                    = self.event_description_scroll.checked_add(1)
                        .unwrap_or(u16::MAX);
            }
            _ => { /* pass */ }
        }
        return None;
    }

    /// Changes any state if needed because view has changed
    fn handle_view_change(&mut self, model : &Model) {
        let new_display_mode = *model.display_mode();
        if Some(new_display_mode) != self.last_display_mode {
            // if leaving view
            match self.last_display_mode {
                Some(DisplayMode::Event) => {
                    self.event_description_scroll = 0;
                }
                _ => { /* pass */ }
            }
        }
        self.last_display_mode = Some(new_display_mode);

        // adjust tt scroll position
        let zoom_level = *model.zoom_level();
        let cursor_hour_row = model.cursor_pos().hour_row;
        if self.last_zoom_level != zoom_level {
            if self.last_zoom_level > 0 {
                let cursor_offset
                    = self.last_cursor_hour_row - self.hour_row_offset;
                self.hour_row_offset = cursor_hour_row
                    .checked_sub(cursor_offset)
                    .unwrap_or(0);
            }
            self.last_zoom_level = zoom_level;
        }
        self.last_cursor_hour_row = cursor_hour_row;
    }
}

/// Optional rects may or may not appear depending on mode
struct MainLayout {
    time_col : Rect,
    day_cols : Rect,
    cal_list : Option<Rect>,
    event_details : Option<Rect>,
    search_input : Option<Rect>
}

pub fn view(model: &Model, view_state: &mut ViewState, frame: &mut Frame) {
    view_state.handle_view_change(model);
    match model.display_mode() {
        DisplayMode::Timetable => view_main(model, view_state, frame),
        DisplayMode::Event => view_event(model, view_state, frame),
        DisplayMode::CalendarList
            => view_calendar_list(model, view_state, frame),
        DisplayMode::Help => view_help(frame),
    }
}

pub fn suggest_zoom_level(model : &Model, frame : &Frame) -> usize {
    let mut num_rows = frame.size().height.checked_sub(2).unwrap_or(0);
    if model.show_info_row {
        num_rows = num_rows.checked_sub(INFO_ROW_SIZE).unwrap_or(0);
    }
    let suggested_zoom = (num_rows / SUGGEST_ZOOM_NUM_HOURS) as usize;
    return cmp::max(1, cmp::min(MAX_ZOOM, suggested_zoom));
}

fn view_main(model : &Model, view_state : &mut ViewState, frame : &mut Frame) {
    let main_layout = make_main_layout(model, view_state, frame);
    make_title(model, frame, main_layout.time_col);
    let days_layout = make_days_layout(model, main_layout.day_cols);
    let num_hour_rows = make_hours_col(
        model, view_state, frame, days_layout[0]
    );
    make_days_columns(
        model, view_state, frame, &days_layout[1..], num_hour_rows
    );
    make_calendar_list(model, view_state, frame, main_layout.cal_list);
    make_event_details(model, view_state, frame, main_layout.event_details);
    make_search_term_input(view_state, frame, main_layout.search_input);
}

fn make_main_layout(
    model : &Model, view_state : &ViewState, frame : &Frame
) -> MainLayout {
    let mut main_constraints = vec![
        Constraint::Length(1),
        Constraint::Min(0),
    ];

    let mut search_input_view = None;
    let mut cal_list_view = None;
    let mut event_details_view = None;

    if view_state.is_entering_search_term {
        main_constraints.push(Constraint::Length(3));
    }

    if model.show_info_row {
        main_constraints.push(Constraint::Length(INFO_ROW_SIZE));
    }

    let main_layout = Layout::new(Direction::Vertical, main_constraints)
        .split(frame.size());

    if view_state.is_entering_search_term {
        search_input_view = Some(main_layout[2]);
    }

    if model.show_info_row {
        let details_panel = Layout::new(
            Direction::Horizontal,
            [ Constraint::Percentage(25), Constraint::Percentage(75) ],
        ).split(main_layout[main_layout.len() - 1]);

        cal_list_view = Some(details_panel[0]);
        event_details_view = Some(details_panel[1]);
    }

    return MainLayout {
        time_col: main_layout[0],
        day_cols: main_layout[1],
        cal_list: cal_list_view,
        event_details: event_details_view,
        search_input: search_input_view,
    };
}

fn make_title(model : &Model, frame : &mut Frame, rect : Rect) {
    let cur_date = model.current_date().format("%B %Y");
    let tracking = if *model.is_tracking_mode() { " [t]" } else { "" };
    frame.render_widget(
        Block::new()
            .borders(Borders::TOP)
            .title(format!("{0}{1}", cur_date, tracking)),
        rect,
    );
}

fn make_days_layout(model : &Model, rect : Rect) -> Rc<[Rect]> {
    let num_days = *model.num_days_visible();
    let mut days_constraints = Vec::with_capacity(1 + num_days);
    days_constraints.push(Constraint::Max(TIME_WIDTH_CHARS));
    for _ in 0..num_days {
        days_constraints.push(Constraint::Min(0));
    }
    let days_layout = Layout::new(Direction::Horizontal, days_constraints)
        .split(rect);

    days_layout
}

/// Draws the times down the side and returns how many rows there are
///
/// Two rows per hour. Sets the view state to scroll current time if needed
/// and makes sure the cursor is on screen by scrolling.
fn make_hours_col(
    model : &Model,
    view_state : &mut ViewState,
    frame : &mut Frame,
    rect : Rect,
) -> usize {
    let hours = Block::new().title("Time");
    let hours_space = hours.inner(rect);
    let num_screen_rows
        = usize::try_from(hours_space.height).unwrap() - 1;

    if *model.is_tracking_mode() {
        view_state.scroll_to_current_time(model, num_screen_rows);
    }
    view_state.scroll_so_cursor_visible(
        num_screen_rows,
        model.cursor_pos().hour_row,
    );

    let max_hour_row = get_max_hour_row(model, view_state, num_screen_rows);
    let hours_layout = Layout::new(
        Direction::Vertical,
        vec![Constraint::Length(1); num_screen_rows],
    ).split(hours_space);

    for row in view_state.hour_row_offset..max_hour_row {
        let layout_index = row - view_state.hour_row_offset;
        let has_cursor = row == model.cursor_pos().hour_row;

        let hour_factor = model.num_hour_rows() / NUM_HOURS_PER_DAY;
        if row % hour_factor == 0 {
            let hour = row / hour_factor;
            let mut paragraph = Paragraph::new(format!("{0}:00",  hour));
            if has_cursor {
                paragraph = paragraph.style(*view_state.colors.cursor_style());
            }
            frame.render_widget(paragraph, hours_layout[layout_index]);
        } else if has_cursor {
            frame.render_widget(
                Block::new().style(*view_state.colors.cursor_style()),
                hours_layout[layout_index],
            );
        }
    }

    num_screen_rows
}

fn make_days_columns(
    model : &Model,
    view_state : &ViewState,
    frame : &mut Frame,
    day_rects : &[Rect],
    num_hour_rows : usize
) {
    for day in 0..*model.num_days_visible() {
        make_day_column(
            model, view_state, frame, day, day_rects[day], num_hour_rows
        );
    }
}

/// Make column for a day
///
/// day is index of day (0 for Mon)
fn make_day_column(
    model : &Model,
    view_state : &ViewState,
    frame : &mut Frame,
    day : usize,
    day_rect : Rect,
    num_hour_rows : usize,
) {
    let day_date = model.current_date().add(Days::new(day as u64));
    let day_block = Block::new()
        .borders(Borders::LEFT)
        .title(
            format!("{0} {1}", DAYS[day], day_date.format("%d"))
        );
    let day_event_space = day_block.inner(day_rect);
    let day_events_layout = Layout::new(
        Direction::Vertical,
        vec![Constraint::Length(1); num_hour_rows],
    ).split(day_event_space);

    frame.render_widget(day_block, day_rect);

    let matrix = model.get_events_matrix(&day_date);
    let max_hour_row = get_max_hour_row(model, view_state, num_hour_rows);
    let matrix = &matrix[view_state.hour_row_offset..max_hour_row];
    let current_date = Local::now().date_naive();
    let cur_time_idx = if current_date == day_date {
        view_state.get_current_time_index(model)
    } else {
        None
    };

    for idx in 0..matrix.len() {
        let events = &matrix[idx];
        let time_slot = Block::new();
        let is_now = Some(idx) == cur_time_idx;

        if events.is_empty() {
            let now_text = if is_now {
                get_cur_time_marker(view_state)
            } else {
                Span::from("")
            };

            let is_cursor = model.cursor_pos().equals(
                day, idx + view_state.hour_row_offset, 0
            );
            let cell_para = if is_cursor {
                Paragraph::new(now_text)
                    .style(*view_state.colors.cursor_style())
            } else {
                Paragraph::new(now_text)
            };
            frame.render_widget(cell_para, day_events_layout[idx]);
            continue;
        }

        let time_slot_space = time_slot.inner(day_events_layout[idx]);
        let max_events_displayable = ((time_slot_space.width + 1) / 2) as usize;
        let num_events_to_display
            = cmp::min(events.len(), max_events_displayable);
        let mut time_slot_constraints: Vec<Constraint>
            = Vec::with_capacity(2 * events.len() - 1);
        for idx in 0..num_events_to_display {
            time_slot_constraints.push(Constraint::Min(0));
            // padding between events
            if idx < events.len() - 1 {
                time_slot_constraints.push(Constraint::Length(1));
            }
        }
        let time_slot_layout = Layout::new(
            Direction::Horizontal, time_slot_constraints
        ).split(time_slot_space);

        let col_cursor_idx = if model.cursor_pos().day_idx == day {
            model.cursor_pos().event_idx
        } else {
            0
        };
        let cursor_is_last = col_cursor_idx == events.len() - 1;
        let last_idx = cmp::min(
            events.len() - 1,
            col_cursor_idx + if cursor_is_last { 0 } else { 1 }
        );
        let display_start = last_idx.checked_sub(max_events_displayable - 1)
            .unwrap_or(0);
        let display_end = display_start + num_events_to_display;
        let events_slice = &events[display_start..display_end];

        for (event_idx, event) in events_slice.iter().enumerate() {
            let true_event_idx = display_start + event_idx;
            // … last or first item if not show all
            if events.len() > max_events_displayable
                && (
                    event_idx == max_events_displayable - 1
                    && true_event_idx < events.len() - 1
                ) || (event_idx == 0 && true_event_idx > 0) {

                frame.render_widget(
                    Paragraph::new("…"),
                    time_slot_layout[2 * event_idx]
                );
                continue;
            }

            let is_cursor = model.cursor_pos().equals(
                day,
                idx + view_state.hour_row_offset,
                true_event_idx,
            );
            let now_text = if is_now && event_idx == 0 {
                get_cur_time_marker(view_state)
            } else {
                Span::from("")
            };

            if !event.is_some() {
                if is_cursor {
                    frame.render_widget(
                        Paragraph::new(now_text)
                            .style(*view_state.colors.cursor_style()),
                        time_slot_layout[2 * event_idx]
                    );
                }
                continue;
            }
            let event = event.as_ref().unwrap();

            let (start_idx, _) = model.get_event_indices(&day_date, event);
            let summary = get_event_summary(event);

            let mut summary_display = &"".to_string();
            // display title on first row or at top of screen for all day events
            if let Some(start_idx) = start_idx {
                let row_idx = start_idx.checked_sub(view_state.hour_row_offset);
                if let Some(row_idx) = row_idx {
                    if row_idx == idx {
                        summary_display = &summary;
                    }
                }
            } else if start_idx.is_none() && idx == 0 {
                summary_display = &summary;
            }

            let style : Style = if is_cursor {
                *view_state.colors.cursor_style()
            } else {
                get_style_from_event(model, view_state, &event)
            };

            frame.render_widget(
                Paragraph::new(
                    Line::from(vec![now_text, Span::from(summary_display)])
                ).style(style),
                time_slot_layout[2 * event_idx]
            );
        }
    }
}

fn get_event_summary(event : &CalendarEvent) -> String {
    return unescape_cal_string(&event.event().get_summary().unwrap_or(""));
}

fn case_insensitive_contains(needle : &String, haystack : &String) -> bool {
    let re = &format!("(?i){}", regex::escape(needle));
    if let Some(re) = Regex::new(re).ok() {
        re.is_match(haystack)
    } else {
        false
    }
}

fn get_cur_time_marker<'a>(view_state : &ViewState) -> Span<'a> {
    Span::styled("*", *view_state.colors.now_style())
}

fn get_max_hour_row(
    model : &Model, view_state : &ViewState, num_hour_rows : usize
) -> usize {
    cmp::min(model.num_hour_rows(), view_state.hour_row_offset + num_hour_rows)
}

fn make_calendar_list(
    model : &Model,
    view_state : &ViewState,
    frame : &mut Frame,
    rect : Option<Rect>,
) {
    if rect.is_none() {
        return;
    }
    let rect = rect.unwrap();

    let items = get_calendar_list_items(model, view_state);

    let col_width = cmp::min(
        items.iter()
            .map(|item| item.width())
            .max()
            .unwrap_or(MAX_CALENDAR_NAME_WIDTH_CHARS) + 1,
        MAX_CALENDAR_NAME_WIDTH_CHARS + 1,
    ) as u16;

    // - borders
    let line_width = rect.width - 2;
    // + bullet icon and space
    let num_cols = line_width / col_width;

    let mut rows : Vec<Row> = Vec::new();
    let mut cur_row : Vec<Line> = Vec::new();
    for cell in items.into_iter() {
        if cur_row.len() >= num_cols.into() {
            rows.push(Row::new(cur_row));
            cur_row = Vec::new();
        }
        cur_row.push(cell);
    };
    rows.push(Row::new(cur_row));

    let widths : Vec<Constraint> = (0..num_cols).map(|_|
        Constraint::Length(col_width)
    ).collect();

    frame.render_widget(
        Table::new(rows, widths)
            .column_spacing(0)
            .block(Block::bordered().title("Calendars")),
        rect
    );
}

fn make_event_details(
    model : &Model,
    view_state : &ViewState,
    frame : &mut Frame,
    rect : Option<Rect>
) {
    if rect.is_none() {
        return;
    }
    let rect = rect.unwrap();

    let cal_event = model.get_event_under_cursor();
    let description = get_event_heading(
        model, view_state, &cal_event, false, true
    );

    frame.render_widget(
        Paragraph::new(description)
            .wrap(Wrap { trim: true })
            .block(Block::bordered().title("Details")),
        rect
    );
}

fn make_search_term_input(
    view_state : &ViewState, frame : &mut Frame, rect : Option<Rect>
) {
    if rect.is_none() {
        return;
    }

    let input = Paragraph::new(view_state.search_term_input.value())
        .block(Block::bordered().title("Enter Search Term"));

    frame.render_widget(input, rect.unwrap());
}

/// Format a time for an event but use override if given
///
/// Will only show time if datetime has a time, else just day
fn format_event_datetime<T : TimeZone>(
    datetime : &Option<DatePerhapsTime>,
    dt_override : &Option<DateTime<T>>,
) -> String
    where T::Offset: Display {
    let local_datetime = dateperhapstime_to_local_datetime(&datetime);
    if let Some(datetime) = datetime {
        let format_str = match datetime {
            IDateTime(_) => "%H:%M, %d %b %y",
            IDate(_) => "%d %b %y",
        };
        if dt_override.is_some() {
            format!("{0}", dt_override.as_ref().unwrap().format(format_str))
        } else {
            format!("{0}", local_datetime.unwrap().format(format_str))
        }
    } else {
        ".".to_string()
    }
}

fn get_style_from_event(
    model : &Model, view_state : &ViewState, event : &CalendarEvent
) -> Style {
    if let Some(term) = model.search_term() {
        let is_match = case_insensitive_contains(
            term, &get_event_summary(event)
        );
        if is_match {
            return get_style_from_calendar_index(
                model, view_state, event.calendar_index()
            );
        } else {
            return *view_state.colors.not_match_style();
        }
    } else {
        return get_style_from_calendar_index(
            model, view_state, event.calendar_index()
        );
    }
}

fn get_style_from_calendar_index(
    model: &Model, view_state : &ViewState, cal_idx : usize
) -> Style {
    let active_idx = if model.is_calendar_active(cal_idx)  { 0 } else { 1 };
    let styles = view_state.colors.calendar_styles();
    styles[cal_idx % styles.len()][active_idx]
}

fn view_event(model : &Model, view_state : &mut ViewState, frame : &mut Frame) {
    let details = Block::bordered().title("Details");
    let inner = details.inner(frame.size());
    let layout = Layout::new(
        Direction::Vertical,
        [
            Constraint::Length(5),
            Constraint::Min(0),
        ],
    ).split(inner);
    let header = layout[0];
    let body = layout[1];

    let body_layout = Layout::new(
        Direction::Horizontal,
        [
            Constraint::Min(0),
            Constraint::Length(1),
            Constraint::Length(1),
        ]
    ).split(body);
    let body_text = body_layout[0];
    let body_scrollbar = body_layout[2];

    let cal_event = model.get_event_under_cursor();
    let heading = get_event_heading(model, view_state, &cal_event, true, false);
    let description = cal_event
        .map(|cal_event| {
            unescape_cal_string(
                cal_event.event().get_description().unwrap_or("")
            )
        }).unwrap_or("".to_string());

    frame.render_widget(details, frame.size());
    frame.render_widget(
        Paragraph::new(heading).wrap(Wrap { trim: true }),
        header,
    );

    let mut body_para = Paragraph::new(description)
        .wrap(Wrap { trim: true });
    let content_height = body_para.line_count(body_text.width);
    let max_scroll = content_height
        .checked_sub(usize::try_from(body_text.height).unwrap_or(usize::MAX))
        .unwrap_or(0);

    view_state.event_description_scroll = cmp::min(
        u16::try_from(max_scroll).unwrap_or(u16::MAX),
        view_state.event_description_scroll
    );

    body_para = body_para.scroll((view_state.event_description_scroll, 0));
    frame.render_widget(body_para, body_text);

    let scrollbar = Scrollbar::new(ScrollbarOrientation::VerticalRight)
        .begin_symbol(Some("↑"))
        .end_symbol(Some("↓"));
    let mut scrollbar_state = ScrollbarState::new(max_scroll)
        .position(
            usize::try_from(view_state.event_description_scroll)
                .unwrap_or(usize::MAX)
        );
    frame.render_stateful_widget(
        scrollbar, body_scrollbar, &mut scrollbar_state
    );
}

/// Returns blank if no event
/// Options to include the name of the calendar and/or the full description
fn get_event_heading<'a>(
    model : &'a Model,
    view_state : &ViewState,
    cal_event : &Option<CalendarEvent<'a>>,
    include_calendar : bool,
    include_description : bool,
) -> Text<'a> {
    let mut heading = Text::from(Span::raw(String::new()));
    if let Some(cal_event) = cal_event {
        let style = get_style_from_event(model, view_state, cal_event);
        let event = cal_event.event();

        let start_string
            = format_event_datetime(&event.get_start(), cal_event.dt_start());
        let end_string
            = format_event_datetime(&event.get_end(), cal_event.dt_end());

        heading = Text::from(vec![
            Line::from(Span::styled(
                get_event_summary(cal_event),
                style,
            )),
            Line::from(Span::raw(format!(
                "From {0} to {1}", start_string, end_string
            ))),
        ]);
        if include_calendar {
            let calendars = model.calendar.get_calendars();
            let calendar = &calendars[cal_event.calendar_index()];
            let calendar_name = get_calendar_name(calendar);
            heading.extend(Line::from(format!("Calendar: {0}", calendar_name)));
        }
        if let Some(location) = event.get_location() {
            heading.extend(Line::from(format!(
                "Location: {0}",
                unescape_cal_string(location),
            )));
        }
        if include_description {
            heading.extend(Text::from(
                unescape_cal_string(event.get_description().unwrap_or("")),
            ));
        }
    }
    return heading;
}

fn view_calendar_list(
    model : &Model, view_state : &ViewState, frame : &mut Frame
) {
    let items = get_calendar_list_items(model, view_state);

    let list = List::new(items)
        .block(Block::bordered().title("Calendars"))
        .highlight_symbol(">>")
        .repeat_highlight_symbol(true);

    let mut list_state = ListState::default();
    list_state.select(Some(*model.calendar_list_cursor_position()));

    frame.render_stateful_widget(list, frame.size(), &mut list_state);
}

/// Get styled list of calendars
///
/// With coloured blocks and other status details
fn get_calendar_list_items<'a>(
    model : &'a Model, view_state : &ViewState
) -> Vec<Line<'a>> {
    model.calendar.get_calendars().iter()
        .map(get_calendar_name)
        .enumerate()
        .map(|(idx, name)| {
            let active_char = if model.is_calendar_active(idx) {
                " "
            } else {
                "|"
            };
            Line::from(vec![
                Span::styled(
                    active_char,
                    get_style_from_calendar_index(model, view_state, idx)
                ),
                Span::raw(" "),
                Span::raw(name),
            ])
        }).collect()
}

fn get_calendar_name(calendar : &ICalendarFile) -> &str {
    let default = &calendar.filename;
    Path::new(&calendar.filename)
        .file_stem()
        .map(|stem| stem.to_str().unwrap_or(default))
        .unwrap_or(default)
}

fn view_help(frame : &mut Frame) {
    let widths = [
        Constraint::Length(9),
        Constraint::Length(1),
        Constraint::Min(0),
    ];

    let rows = [
        Row::new([ "h,j,k,l", ":", "move cursor" ]),
        Row::new([ "arrows", ":", "move cursor" ]),
        Row::new([ "H,L", ":", "prev/next week" ]),
        Row::new([ "PgUp/Down", ":", "prev/next week" ]),
        Row::new([ "t,home", ":", "track current time (cancelled by scroll)" ]),
        Row::new([ "d", ":", "show/hide details panels" ]),
        Row::new([ "w", ":", "toggle full/working week view" ]),
        Row::new([ "enter", ":", "view selected event" ]),
        Row::new([ "c,v", ":", "show calendar selection list" ]),
        Row::new([ "r", ":", "reload calendars" ]),
        Row::new([ "esc,q,,", ":", "back/quit" ]),
        Row::new([ "z", ":", "next zoom level" ]),
        Row::new([ "/", ":", "enter search term" ]),
        Row::new([ "?", ":",  "help" ]),
    ];

    frame.render_widget(
        Table::new(rows, widths)
            .block(Block::bordered().title("Help")),
        frame.size(),
    );
}
