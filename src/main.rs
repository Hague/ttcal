
use chrono::Timelike;
use chrono::offset::Local;
use colors::Colors;
use termbg::Theme;
use std::env;
use std::io::{self, stdout};

use crossterm::{
    event::{self, Event},
    terminal::{
        disable_raw_mode,
        enable_raw_mode,
        EnterAlternateScreen,
        LeaveAlternateScreen
    },
    ExecutableCommand,
};
use ratatui::prelude::*;

use crate::keymap::{KEYMAP, UpdateMsg};
use crate::model::Model;
use crate::view::{suggest_zoom_level, view, ViewState};

mod calendar;
mod colors;
mod keymap;
mod model;
mod view;

fn main() -> io::Result<()> {
    let args: Vec<String> = env::args().collect();

    if args.contains(&String::from("-h"))
        || args.contains(&String::from("-H")) {
        println!(
            "Usage: ttcal calendar1.ics calendar2.ics ...\n\
            In the app, press ? for help."
        );
        return Ok(());
    }

    while run_calendar(&args)? { }

    return Ok(());
}

/// Returns true if should reload (else quit)
fn run_calendar(args : &Vec<String>) -> io::Result<bool> {
    let timeout = std::time::Duration::from_millis(100);


    println!("Loading calendars...");
    let model_resources = if args.len() < 2 {
            Ok(Model::default_resources())
        } else {
            let cal_files= &args[1..];
            Model::get_resources_from_files(cal_files)
        }?;
    let mut model = Model::new(&model_resources);


    let theme = termbg::theme(timeout).ok().unwrap_or(Theme::Light);
    let mut view_state = ViewState::new(Colors::new(theme));

    enable_raw_mode()?;
    stdout().execute(EnterAlternateScreen)?;
    let mut terminal = Terminal::new(CrosstermBackend::new(stdout()))?;

    terminal.draw(|frame| {
        model.set_zoom_level(suggest_zoom_level(&model, frame));
    })?;

    'main: loop {
        terminal.draw(|frame| view(&model, &mut view_state, frame))?;
        let mut message = handle_events(&model, &mut view_state)?;

        while message.is_some() {
            match message.unwrap() {
                UpdateMsg::ModelMsg(mmsg) => {
                    if mmsg == model::Message::Quit {
                        break 'main;
                    }
                    message = model.update(mmsg)
                        .map(|m| UpdateMsg::ModelMsg(m));
                }
                UpdateMsg::ViewMsg(vmsg) => {
                    message = view_state.update(&model, vmsg)
                        .map(|m| UpdateMsg::ModelMsg(m));
                }
                UpdateMsg::Reload => {
                    disable_raw_mode()?;
                    stdout().execute(LeaveAlternateScreen)?;
                    return Ok(true);
                }
            }
        }
    }

    disable_raw_mode()?;
    stdout().execute(LeaveAlternateScreen)?;
    Ok(false)
}

fn handle_events(
    model : &Model, view_state : &mut ViewState
) -> io::Result<Option<UpdateMsg>> {
    // update on the half hour so current time marker is up to date
    if event::poll(std::time::Duration::from_secs(seconds_to_half_hour()))? {
        if let Event::Key(key) = event::read()? {
            if view_state.is_consuming_input() {
                return Ok(
                    view_state.consume_input(key)
                        .map(|msg| UpdateMsg::ModelMsg(msg))
                );
            } else {
                return Ok(
                    KEYMAP.get(model.display_mode())
                        .map(|keys| keys.get(&key).map(|v| v.clone()))
                        .flatten()
                );
            }
        }
    } else {
        return Ok(Some(UpdateMsg::ModelMsg(model::Message::Refresh)));
    }
    return Ok(None)
}

fn seconds_to_half_hour() -> u64 {
    let current_time = Local::now().time();
    let hour_seconds = current_time.minute() * 60 + current_time.second();
    let from_half_hour = hour_seconds % (30 * 60);
    return ((30 * 60) - from_half_hour).into();
}
